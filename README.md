# Energy for Life

The main repository of [Energy for Life] project.
It is here we collect ideas, and feedback from people who take care about how we produce and consume energy. 

[Energy for Life]: https://e4l.uni.lu

## Your contribution matters

More information about contributions you can find in [CONTRIBUTING.md] file.

[CONTRIBUTING.md]: https://gitlab.com/uniluxembourg/fstm/dcs/open/e4l/lu.uni.e4l.platform.documentation/-/blob/master/CONTRIBUTING.md
